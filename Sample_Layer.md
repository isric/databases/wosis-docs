---
title: Sample & Layer
author: Luís Moreira de Sousa luis[dot]desousa[at]isric[dot]org 
date: 2023-11-01
---

Sample & Layer
==============

These are few of the concepts that echoed with international standards in the
original WoSIS data model.  Sample corresponds to a physical mass of soil
material, possibly stored at an identifiable location. Often, samples include
soil material collected at different, but geographically close, plots. This to
smooth out local variations in soil composition. A Layer is an arbitrary segment
of a soil profile, habitually defined in the context of soil surveys (e.g. to
distinguish between top-soil and sub-soil).

These two concepts are not easy to distinguish in some international standards,
most notably in ISO-28258. Layers often correspond to depth intervals determined
in soil surveys, assigned to the same set observable physio-chemical properties.
However, in WoSIS the two concepts are relatively well defined, with Sample
solely identifying soil material and Layer expanded to encompass pedo-genetic
information. This means a layer record in WoSIS can actually correspond to a
soil horizon.

New data model
--------------

The main modification to the data model is the detachment of the `layer` table
from `sample`. Both tables now refer directly to the `profile` table and are
managed independently. A new sample may be recorded without a layer record and
vice-versa. As consequence, results became associated solely with the `layer`
table. 


```mermaid
erDiagram
  layer {
    INTEGER layer_id
    INTEGER profile_id
    SMALLINT layer_number
    VARCHAR layer_name
    SMALLINT upper_depth
    SMALLINT lower_depth
    BOOLEAN organic_surface
  }
  sample {
    INTEGER sample_id
    INTEGER profile_id
    VARCHAR type
    VARCHAR code
    SMALLINT composition
    BOOLEAN available
    SMALLINT upper_depth
    SMALLINT lower_depth
  }
  profile {
    INTEGER dataset_id
    INTEGER site_id
    INTEGER thesaurus_trust_id
    TEXT profile_code
    SMALLINT year
    SMALLINT month
    SMALLINT day
    INTEGER profile_old_id
    INTEGER profile_id
  }
  "wosis_source.layer" {
    INTEGER layer_id
    VARCHAR layer_name_source
    VARCHAR upper_depth_source
    VARCHAR lower_depth_source
  }
  "wosis_source.layer" ||--o{ layer : layer_id_fkey
  layer ||--o{ profile : layer_profile_id_fkey
  sample ||--o{ profile : sample_profile_id_fkey
```

### Tables in the data model 

- `layer`: Layer. A vertical profile element considered to have an
  heterogeneous composition. 

- `sample`: A soil sample, possibly still available in a physical collection.





