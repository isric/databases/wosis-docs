---
title: Main features of interest
author: Luís Moreira de Sousa luis[dot]desousa[at]isric[dot]org 
date: 2023-08-06
---

Main Features of Interest
=========================

The WoSIS data model (vr. 2023) identifies the features of interest and other umbrella
concepts, with important changes to nomenclature.
The features of interest approximate to the extent possible the concepts in
ISO-28258, INSPIRE Soil Theme and OGC SoilIE.


Relevant tables in the data model
---------------------------------

- `dataset`: This table retains the same meaning, representing a single source
  of data. Examples may be the LUCAS programme in Europe, or the Soils4Africa
  project. The same source of data may include information gathered in different
  surveys, or collected at different points in time. Recurring surveys such as
  the LUCAS programme in Europe result in single dataset.

- `site`: geo-spatial location where a soil investigation took place, or takes
  place (in recurring surveys). Site is the only geo-spatial feature in WoSIS
  referenced by the soil survey records. In the original data model this concept
  was called `profile`, at odds with international standards. Each site should
  declare a geometry, but since in the legacy data many sites do not have one,
  this restriction is not enforced. Geometry functions can be employed to
  determine in which country or continent a site lays.

- `profile`: an ordered stack of pedo-genetic horizons along the depth of the
  soil. A profile can also be a stack of ordered layers (arbitrary depth
  intervals unrelated to pedo-genesis). A profile can be classified according to
  a soil classification system (e.g. WRB, FAO) and be the feature of interest in
  various observations. Since the concepts of surface and plot do not exist in
  WoSIS, properties associated with those features in international
  standards are instead associated with profile in this database. Each profile
  results from a single investigation on a single site, hence the date fields in
  this table. For instance, if the same site is surveyed in different years in
  the LUCAS project, two different profiles should result, associated to the
  same site and same dataset. This table was named `dataset_profile` in the
  original data model ([Ribeiro et al. 2020](http://dx.doi.org/10.17027/isric-wdc-2020-01)).

```mermaid
erDiagram
  thesaurus_trust {
    INTEGER thesaurus_trust_id
    VARCHAR value
    VARCHAR definition
  }
  profile {
    INTEGER profile_id
    INTEGER dataset_id
    INTEGER site_id
    INTEGER thesaurus_trust_id
    TEXT profile_code
    SMALLINT year
    SMALLINT month
    SMALLINT day
  }
  site {
    INTEGER site_id
    NULL geom
    REAL geom_accuracy
    VARCHAR geom_accuracy_type
    BOOLEAN hide
  }
  dataset {
    INTEGER dataset_id
    TEXT dataset_code
    TEXT version
    TEXT dataset_progress
    TEXT dataset_type
    SMALLINT dataset_rank
    BOOLEAN licence_wosis
    BOOLEAN licence_soilgrids
    TEXT licence_file
    TEXT legal_constraints
    DATE publication_date
    TEXT project
  }
  profile ||--o{ dataset : dataset_profile_dataset_id_fkey
  profile ||--o{ thesaurus_trust : fk_thesaurus_trust
  profile ||--o{ site : profile_site_id_fkey
```

