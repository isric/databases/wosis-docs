---
title: Site
author: Luís Moreira de Sousa luis[dot]desousa[at]isric[dot]org 
date: 2023-10-11
---

Site
====

The concept of Site can be described as a location in which a soil profile is
described or otherwise characterised. The same site can be visited at different
moments in time and feature in different datasets. Thus multiple different
profiles may exist for a same site.

Site versus Plot
----------------

It is important to stress the option for Site and not Plot. WoSIS is a global
dataset, resulting from a collation of different data sources. It should _not be
expected to convey the overall positional accuracy of a local or regional
soil survey_. Specific aspects in the way geo-spatial position is recorded justify
this option:

- In the vast majority of sites the positional accuracy is recorded as
  ~ 100 metre uncertainty since no specific information is recorded on instruments and local corrections applied in most source datasets.

- Positional accuracy in the 'older' source datasets is recorded in decimal degrees, not in metres. This points
  to a low accuracy methodology recording spatial features.

- The WGS84 epoch is not recorded. Especially around the South Pacific the
  geographic coordinates recorded in WoSIS may represent on the latest
realisation of the WGS84 system a location several metres apart from the
location originally sampled/described.
   
In conclusion, although represented by a point geometry, a site is in effect an
area of extent in which a soil investigation was conducted.


Data model
--------------

Beyond the essential identification of the spatial feature Site, other relevant
changes in the data model include the severance of a direct reference from the
spatial feature to the `country` table. This relation can be established
spatially. The `country` table itself was normalised, with the encapsulation
of the Continent and Region concepts in dedicated tables (`continent` and
`country_region`). 


``` mermaid
erDiagram
  country_geom {
    INTEGER country_geom_id
    INTEGER country_id
    NULL geom
    NULL geom_label
  }
  continent {
    INTEGER continent_id
    VARCHAR name
    VARCHAR name_custom
  }
  country_region {
    INTEGER country_region_id
    VARCHAR name
    VARCHAR note
    INTEGER continent_id
  }
  country {
    INTEGER country_id
    VARCHAR country_code
    VARCHAR iso3_code
    VARCHAR colour_code
    VARCHAR gaul_code
    VARCHAR en
    VARCHAR ar
    VARCHAR es
    VARCHAR fr
    VARCHAR pt
    VARCHAR ru
    VARCHAR zh
    VARCHAR status
    BOOLEAN disp_area
    VARCHAR capital
    INTEGER country_region_id
  }
  profile {
    INTEGER dataset_id
    INTEGER site_id
    INTEGER thesaurus_trust_id
    TEXT profile_code
    SMALLINT year
    SMALLINT month
    SMALLINT day
    INTEGER profile_old_id
    INTEGER profile_id
  }
  site {
    INTEGER site_id
    NULL geom
    REAL geom_accuracy
    VARCHAR geom_accuracy_type
    BOOLEAN hide
  }
  profile ||--o{ site : profile_site_id_fkey
  country ||--o{ country_region : country_region_id_fkey
  country_geom ||--o{ country : country_geom_country_id_fkey
  country_region ||--o{ continent : continent_id_fkey
```

### Tables in the data model

- `site`: Location in which a soil profile is described or otherwise characterised. The same Site can be visited at different moments in time and feature in different datasets. Thus various different profiles may be associated to a Site. 

- `country`: Global Administrative Unit Layers (GAUL) from FAO and ISO 3166
  International Standard country codes.

- `country_geom`: Geometry from Global Administrative Unit Layers (GAUL) from
  FAO and ISO 3166 International Standard country codes.

- `country_region`: Collection of regions with respective names according to the
  UN.

- `continent`: Collection of continents with respective names.

