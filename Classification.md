---
title: Soil Classification Systems
author: Luís Moreira de Sousa luis[dot]desousa[at]isric[dot]org 
date: 2023-11-23
---

Classification Systems in the new Data Model
============================================

WoSIS records soil profile classifications according to three different international systems:
- [FAO/UNESCO Legend of the Soil Map of the World](https://www.fao.org/soils-portal/data-hub/soil-classification/fao-legend/en/)
- [World Reference Base (WRB) of Soil Resources](https://www.fao.org/soils-portal/data-hub/soil-classification/world-reference-base/en/)
- [USDA Soil Taxonomy](https://www.nrcs.usda.gov/resources/guides-and-instructions/soil-taxonomy)

Each of these systems has existed for decades, with various editions published
through time. In most cases these different editions are not (fully) backward
compatible. 


New data Model
--------------

For each classification tier or grade two tables exist: a thesaurus with valid terms and
a many-to-many relationship between that thesaurus and a profile classification.
The latter are identified by the `_profile` suffix in the table name. The
relationship table concerning the topmost tier of the system provides the
reference to the classified profile. In the case of the WRB this is the
`class_wrb_profile` table, to which all other many-to-many relations refer. 


### WRB

The following WRB classification tiers are recorded in WoSIS, thus featuring in
the new data model:
- Reference Soil Group
- Qualifier
- Qualifier position
- Specifier
- Diagnostic property
- Diagnostic material
- Diagnostic horizon

```mermaid
erDiagram
  class_wrb_qualifier_profile {
    INTEGER profile_id
    SMALLINT publication_year
    VARCHAR reference_soil_group_code
    VARCHAR qualifier
    VARCHAR position
    SMALLINT place
    VARCHAR specifier
  }
  class_wrb_horizon {
    SMALLINT publication_year
    VARCHAR horizon
  }
  class_wrb_profile {
    INTEGER profile_id
    SMALLINT publication_year
    VARCHAR reference_soil_group_code
    TIMESTAMP verified_date
    INTEGER thesaurus_trust_id
    TEXT correlation_system
    SMALLINT correlation_year
  }
  class_wrb_profile_verifier {
    INTEGER verifier_id
    INTEGER profile_id
    SMALLINT publication_year
    VARCHAR reference_soil_group_code
  }
  thesaurus_trust {
    INTEGER thesaurus_trust_id
    VARCHAR value
    VARCHAR definition
  }
  class_wrb_rsg {
    SMALLINT publication_year
    VARCHAR reference_soil_group_code
    VARCHAR reference_soil_group
  }
  class_wrb_property_profile {
    INTEGER profile_id
    SMALLINT publication_year
    VARCHAR reference_soil_group_code
    VARCHAR property
    SMALLINT upper_depth
    SMALLINT lower_depth
  }
  profile {
    INTEGER profile_id
    INTEGER dataset_id
    INTEGER site_id
    INTEGER thesaurus_trust_id
    TEXT profile_code
    SMALLINT year
    SMALLINT month
    SMALLINT day
    INTEGER profile_old_id
  }
  class_wrb_material_profile {
    INTEGER profile_id
    SMALLINT publication_year
    VARCHAR reference_soil_group_code
    VARCHAR material
    SMALLINT upper_depth
    SMALLINT lower_depth
  }
  class_wrb_material {
    SMALLINT publication_year
    VARCHAR material
  }
  class_wrb_specifier {
    SMALLINT publication_year
    VARCHAR specifier
  }
  class_wrb_qualifier {
    SMALLINT publication_year
    VARCHAR qualifier
  }
  class_verifier {
    INTEGER verifier_id
    VARCHAR name
    VARCHAR e_mail
  }
  class_wrb_property {
    SMALLINT publication_year
    VARCHAR property
  }
  class_wrb_horizon_profile {
    INTEGER profile_id
    SMALLINT publication_year
    VARCHAR reference_soil_group_code
    VARCHAR horizon
    SMALLINT upper_depth
    SMALLINT lower_depth
  }
  class_wrb_profile ||--o{ thesaurus_trust : class_wrb_profile_thesaurus_trust_id_fkey
  class_wrb_profile ||--o{ class_wrb_rsg : class_wrb_profile_publication_year_reference_soil_group_co_fkey
  class_wrb_profile ||--o{ class_wrb_rsg : class_wrb_profile_publication_year_reference_soil_group_co_fkey
  class_wrb_profile ||--o{ profile : class_wrb_profile_profile_id_fkey
  class_wrb_horizon_profile ||--o{ class_wrb_profile : class_wrb_horizon_profile_profile_id_publication_year_refe_fkey
  class_wrb_horizon_profile ||--o{ class_wrb_horizon : class_wrb_horizon_profile_publication_year_horizon_fkey
  class_wrb_horizon_profile ||--o{ class_wrb_profile : class_wrb_horizon_profile_profile_id_publication_year_refe_fkey
  class_wrb_horizon_profile ||--o{ class_wrb_profile : class_wrb_horizon_profile_profile_id_publication_year_refe_fkey
  class_wrb_horizon_profile ||--o{ class_wrb_horizon : class_wrb_horizon_profile_publication_year_horizon_fkey
  class_wrb_material_profile ||--o{ class_wrb_profile : class_wrb_material_profile_profile_id_publication_year_ref_fkey
  class_wrb_material_profile ||--o{ class_wrb_material : class_wrb_material_profile_publication_year_material_fkey
  class_wrb_material_profile ||--o{ class_wrb_profile : class_wrb_material_profile_profile_id_publication_year_ref_fkey
  class_wrb_material_profile ||--o{ class_wrb_profile : class_wrb_material_profile_profile_id_publication_year_ref_fkey
  class_wrb_material_profile ||--o{ class_wrb_material : class_wrb_material_profile_publication_year_material_fkey
  class_wrb_profile_verifier ||--o{ class_wrb_profile : class_wrb_profile_verifier_profile_id_publication_year_ref_fkey
  class_wrb_profile_verifier ||--o{ class_wrb_profile : class_wrb_profile_verifier_profile_id_publication_year_ref_fkey
  class_wrb_profile_verifier ||--o{ class_verifier : class_wrb_profile_verifier_verifier_id_fkey
  class_wrb_profile_verifier ||--o{ class_wrb_profile : class_wrb_profile_verifier_profile_id_publication_year_ref_fkey
  class_wrb_property_profile ||--o{ class_wrb_property : class_wrb_property_profile_publication_year_property_fkey
  class_wrb_property_profile ||--o{ class_wrb_profile : class_wrb_property_profile_profile_id_publication_year_ref_fkey
  class_wrb_property_profile ||--o{ class_wrb_profile : class_wrb_property_profile_profile_id_publication_year_ref_fkey
  class_wrb_property_profile ||--o{ class_wrb_property : class_wrb_property_profile_publication_year_property_fkey
  class_wrb_property_profile ||--o{ class_wrb_profile : class_wrb_property_profile_profile_id_publication_year_ref_fkey
  class_wrb_qualifier_profile ||--o{ class_wrb_specifier : class_wrb_qualifier_profile_publication_year_specifier_fkey
  class_wrb_qualifier_profile ||--o{ class_wrb_profile : class_wrb_qualifier_profile_profile_id_publication_year_re_fkey
  class_wrb_qualifier_profile ||--o{ class_wrb_profile : class_wrb_qualifier_profile_profile_id_publication_year_re_fkey
  class_wrb_qualifier_profile ||--o{ class_wrb_qualifier : class_wrb_qualifier_profile_publication_year_qualifier_fkey
  class_wrb_qualifier_profile ||--o{ class_wrb_profile : class_wrb_qualifier_profile_profile_id_publication_year_re_fkey
  class_wrb_qualifier_profile ||--o{ class_wrb_qualifier : class_wrb_qualifier_profile_publication_year_qualifier_fkey
  class_wrb_qualifier_profile ||--o{ class_wrb_specifier : class_wrb_qualifier_profile_publication_year_specifier_fkey
```

#### Tables for the WRB

- `wosis_new.class_verifier`: Person responsible for verifying a profile classification with the WRB system.
- `wosis_new.class_wrb_horizon`: Code-lists for diagnostic horizons in each edition of the WRB system.
- `wosis_new.class_wrb_horizon_profile`: Identifies the occurrence of a diagnostic horizon within a soil profile classified with the WRB system.
- `wosis_new.class_wrb_material`: Code-lists for diagnostic materials in each edition of the WRB system.
- `wosis_new.class_wrb_material_profile`: Identifies the occurrence of a diagnostic material within a soil profile classified with the WRB system.
- `wosis_new.class_wrb_profile`: Soil profile classification according to the World Reference Base for Soil Resources.
- `wosis_new.class_wrb_profile_verifier`: Associates a profile classification with one or more persons responsible for its verification.
- `wosis_new.class_wrb_property`: Code-lists for diagnostic properties in each edition of the WRB system.
- `wosis_new.class_wrb_property_profile`: Identifies the occurrence of a diagnostic property within a soil profile classified with the WRB system.
- `wosis_new.class_wrb_qualifier`: Code-lists for qualifiers in each edition of the WRB system.
- `wosis_new.class_wrb_qualifier_position`: Thesauri with admissible qualifier positions for each WRB edition.
- `wosis_new.class_wrb_qualifier_profile`: Associates a qualifier to a soil profile classified with the WRB system.
- `wosis_new.class_wrb_rsg`: Code-lists for WRB Reference Soil Groups and respective codes for each edition of the system.
- `wosis_new.class_wrb_specifier`: Code-lists for specifiers in each edition of the WRB system.



### FAO Legend

The following tiers are recorded in WoSIS for the FAO Legend:
- Major Group
- Soil Unit
- Sub-unit
- Phase
- Diagnostic property
- Diagnostic horizon



```mermaid
erDiagram
  class_fao_subunit {
    SMALLINT publication_year
    VARCHAR subunit
  }
  class_fao_profile {
    INTEGER profile_id
    SMALLINT publication_year
    VARCHAR major_group_code
    VARCHAR soil_unit
    VARCHAR subunit
    VARCHAR phase_code
    TIMESTAMP verified_date
    INTEGER verifier_id
    INTEGER thesaurus_trust_id
  }
  class_verifier {
    INTEGER verifier_id
    VARCHAR name
    VARCHAR e_mail
  }
  class_fao_diagnostic_property_profile {
    INTEGER profile_id
    SMALLINT publication_year
    VARCHAR diagnostic_property
    SMALLINT upper_depth
    SMALLINT lower_depth
  }
  class_fao_diagnostic_property {
    SMALLINT publication_year
    VARCHAR diagnostic_property
  }
  class_fao_phase {
    SMALLINT publication_year
    VARCHAR phase
    VARCHAR phase_code
  }
  thesaurus_trust {
    INTEGER thesaurus_trust_id
    VARCHAR value
    VARCHAR definition
  }
  class_fao_diagnostic_horizon_profile {
    INTEGER profile_id
    SMALLINT publication_year
    VARCHAR diagnostic_horizon
    VARCHAR horizon
    SMALLINT upper_depth
    SMALLINT lower_depth
  }
  class_fao_diagnostic_horizon {
    SMALLINT publication_year
    VARCHAR diagnostic_horizon
  }
  profile {
    INTEGER profile_id
    INTEGER dataset_id
    INTEGER site_id
    INTEGER thesaurus_trust_id
    TEXT profile_code
    SMALLINT year
    SMALLINT month
    SMALLINT day
    INTEGER profile_old_id
  }
  class_fao_major_group {
    SMALLINT publication_year
    VARCHAR major_group_code
    VARCHAR major_group
  }
  class_fao_soil_unit {
    SMALLINT publication_year
    VARCHAR soil_unit_code
    VARCHAR soil_unit
  }
  class_fao_profile ||--o{ class_fao_subunit : class_fao_profile_publication_year_subunit_fkey
  class_fao_profile ||--o{ class_verifier : class_fao_profile_verifier_id_fkey
  class_fao_profile ||--o{ class_fao_phase : class_fao_profile_publication_year_phase_code_fkey
  class_fao_profile ||--o{ class_fao_subunit : class_fao_profile_publication_year_subunit_fkey
  class_fao_profile ||--o{ class_fao_major_group : class_fao_profile_publication_year_major_group_code_fkey
  class_fao_profile ||--o{ class_fao_phase : class_fao_profile_publication_year_phase_code_fkey
  class_fao_profile ||--o{ profile : class_fao_profile_profile_id_fkey
  class_fao_profile ||--o{ thesaurus_trust : class_fao_profile_thesaurus_trust_id_fkey
  class_fao_profile ||--o{ class_fao_soil_unit : class_fao_profile_publication_year_soil_unit_fkey
  class_fao_profile ||--o{ class_fao_major_group : class_fao_profile_publication_year_major_group_code_fkey
  class_fao_profile ||--o{ class_fao_soil_unit : class_fao_profile_publication_year_soil_unit_fkey
  class_fao_diagnostic_horizon_profile ||--o{ class_fao_diagnostic_horizon : class_fao_profile_diagnostic__publication_year_diagnostic__fkey
  class_fao_diagnostic_horizon_profile ||--o{ class_fao_profile : class_fao_profile_diagnostic_h_profile_id_publication_year_fkey
  class_fao_diagnostic_horizon_profile ||--o{ class_fao_diagnostic_horizon : class_fao_profile_diagnostic__publication_year_diagnostic__fkey
  class_fao_diagnostic_horizon_profile ||--o{ class_fao_profile : class_fao_profile_diagnostic_h_profile_id_publication_year_fkey
  class_fao_diagnostic_property_profile ||--o{ class_fao_profile : class_fao_profile_diagnostic_p_profile_id_publication_year_fkey
  class_fao_diagnostic_property_profile ||--o{ class_fao_diagnostic_property : class_fao_profile_diagnostic_publication_year_diagnostic__fkey1
  class_fao_diagnostic_property_profile ||--o{ class_fao_diagnostic_property : class_fao_profile_diagnostic_publication_year_diagnostic__fkey1
  class_fao_diagnostic_property_profile ||--o{ class_fao_profile : class_fao_profile_diagnostic_p_profile_id_publication_year_fkey
```

#### Tables for the FAO Legend

- `class_fao_diagnostic_horizon`: Thesaurus for the diagnostic horizons of the FAO legend.
- `class_fao_diagnostic_property`: Thesaurus for the diagnostic properties of the FAO legend.
- `class_fao_major_group`: Thesaurus for the FAO legend major groups and respective codes for each edition of the legend.
- `class_fao_phase`: Thesaurus for the FAO legend phases and respective codes for each edition of the legend. 
- `class_fao_profile`: Soil profile classification according to the FAO/UNESCO legend of the Soil Map of the World. 
- `class_fao_diagnostic_horizon_profile`: Diagnostic horizons of the FAO legend associated with a classified profile.
- `class_fao_diagnostic_property_profile`: Diagnostic properties of the FAO legend associated with a classified profile.
- `class_fao_soil_unit`: Thesaurus for the FAO legend soil units and respective codes for each edition of the legend. 
- `class_fao_subunit`: Thesaurus for the FAO legend subunits and respective codes for each edition of the legend.



### USDA Soil Taxonomy

The following tiers are recorded in WoSIS for the USDA Soil Taxonomy:
- Order
- Suborder
- Great group
- Subgroup
- Soil temperature regime
- Soil moisture regime
- Mineralogy
- Textural class
- Additional taxons


```mermaid
erDiagram
  class_usda_moisture_regime {
    INTEGER moisture_regime_id
    VARCHAR moisture_regime
  }
  class_usda_additional_profile {
    INTEGER class_usda_profile_id
    INTEGER additional_id
  }
  class_usda_temperature_regime {
    INTEGER temperature_regime_id
    VARCHAR temperature_regime
  }
  thesaurus_trust {
    INTEGER thesaurus_trust_id
    VARCHAR value
    VARCHAR definition
  }
  class_usda_mineralogy {
    INTEGER mineralogy_id
    VARCHAR mineralogy
  }
  class_usda_textural_class {
    INTEGER textural_class_id
    VARCHAR textural_class
  }
  class_usda_great_group {
    INTEGER great_group_id
    VARCHAR great_group
  }
  class_usda_suborder {
    INTEGER suborder_id
    VARCHAR suborder
  }
  profile {
    INTEGER profile_id
    INTEGER dataset_id
    INTEGER site_id
    INTEGER thesaurus_trust_id
    TEXT profile_code
    SMALLINT year
    SMALLINT month
    SMALLINT day
    INTEGER profile_old_id
  }
  class_usda_order_name {
    INTEGER order_name_id
    VARCHAR order_name
  }
  class_usda_subgroup {
    INTEGER subgroup_id
    VARCHAR subgroup
  }
  class_usda_additional {
    INTEGER additional_id
    VARCHAR additional
  }
  class_usda_profile {
    INTEGER class_usda_profile_id
    SMALLINT publication_year
    INTEGER profile_id
    INTEGER order_name_id
    INTEGER suborder_id
    INTEGER great_group_id
    INTEGER subgroup_id
    INTEGER temperature_regime_id
    INTEGER moisture_regime_id
    INTEGER textural_class_id
    INTEGER thesaurus_trust_id
  }
  class_usda_mineralogy_profile {
    INTEGER class_usda_profile_id
    INTEGER mineralogy_id
  }
  class_usda_profile ||--o{ class_usda_textural_class : textural_class_fkey
  class_usda_profile ||--o{ class_usda_moisture_regime : moisture_regime_fkey
  class_usda_profile ||--o{ class_usda_subgroup : subgroup_fkey
  class_usda_profile ||--o{ class_usda_order_name : order_name_fkey
  class_usda_profile ||--o{ class_usda_suborder : suborder_fkey
  class_usda_profile ||--o{ class_usda_great_group : great_group_fkey
  class_usda_profile ||--o{ thesaurus_trust : thesaurus_trust_fkey
  class_usda_profile ||--o{ class_usda_temperature_regime : temperature_regime_fkey
  class_usda_profile ||--o{ profile : profile_fkey
  class_usda_additional_profile ||--o{ class_usda_profile : class_usda_profile_fkey
  class_usda_additional_profile ||--o{ class_usda_additional : class_usda_additional_fkey
  class_usda_mineralogy_profile ||--o{ class_usda_mineralogy : class_usda_mineralogy_fkey
  class_usda_mineralogy_profile ||--o{ class_usda_profile : class_usda_profile_fkey

```

#### Tables for the USDA Taxonomy

- `class_usda_additional`: Thesaurus for additional taxons in the USDA Soil Taxonomy.
- `class_usda_great_group`: Thesaurus for great group names in the USDA Soil Taxonomy.
- `class_usda_mineralogy`: Thesaurus for mineralogy classes in the USDA Soil Taxonomy.
- `class_usda_order_name`: Thesaurus for order names in the USDA Soil Taxonomy.
- `class_usda_profile`: Classification of soil profiles in the USDA Soil Taxonomy.
- `class_usda_additional_profile`: Relationship between classified profile and additional taxons in the USDA Soil Taxonomy.
- `class_usda_mineralogy_profile`: Relationship between classified profile and mineralogy classes in the USDA Soil Taxonomy.
- `class_usda_subgroup`: Thesaurus for subgroup names in the USDA Soil Taxonomy.
- `class_usda_suborder`: Thesaurus for suborder names in the USDA Soil Taxonomy.
- `class_usda_temperature_regime`: Thesaurus for temperature regimes in the USDA Soil Taxonomy.
- `class_usda_textural_class`: Thesaurus for textural classes in the USDA Soil Taxonomy.
- `class_usda_moisture_regime`: Thesaurus for moisture regimes in the USDA Soil Taxonomy.

### Local classification systems

Only two tables feature in this segment of the data model, with all content
recorded as text, without any thesauri. These data are not accurate enough for
use outside the context of WoSIS.

```mermaid
erDiagram
  thesaurus_trust {
    VARCHAR value
    VARCHAR definition
    INTEGER thesaurus_trust_id
  }
  class_local {
    INTEGER class_local_id
    INTEGER profile_id
    INTEGER class_local_system_id
    TEXT classification_name
    TEXT common_name
    TEXT note
    INTEGER thesaurus_trust_id
  }
  class_local_system {
    INTEGER class_local_system_id
    VARCHAR system_name
    SMALLINT publication_year
  }
  profile {
    INTEGER dataset_id
    INTEGER site_id
    INTEGER thesaurus_trust_id
    TEXT profile_code
    SMALLINT year
    SMALLINT month
    SMALLINT day
    INTEGER profile_old_id
    INTEGER profile_id
  }
  class_local ||--o{ profile : class_local_profile_id_fkey
  class_local ||--o{ class_local_system : class_local_system_id_fkey
  class_local ||--o{ thesaurus_trust : thesaurus_trust_id_fkey
```

#### Tables for local classification systems

- `wosis_new.class_local`: Soil name according to the national soil classification system.
- `wosis_new.class_local_system`: Regional or local classification system. 





