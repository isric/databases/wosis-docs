---
title: Analytical Methods
author: Luís Moreira de Sousa luis[dot]desousa[at]isric[dot]org 
date: 2023-08-30
---

Analytical Methods
==================

The concept of analytical method in WoSIS specialises the procedure used to perform an
observation in the laboratory, and contains also information on sample
(pre-)processing and other material handling details. As such it follows the USDA concept of 'operational definitions'.

At the relational level an analytical method is a collection of method options.
The latter is a key-value pair, defining a particular aspect of the overall
method. Specific keys are defined for a sub-group of numerical properties for
layers in the WOSIS Procedures Manual. However, there is an overlap with the
procedure, since values available for particular keys must match the procedure
declared in the observation for a result. This is particularly visible in
properties such as pH or bulk density.

The properties identified in the Procedures Manual (https://doi.org/10.17027/isric-1dq0-1m83) for which analytical methods can be recorded so far are:
- Bulk density fine earth
- Bulk density whole soil
- Calcium carbonate equivalent (TCEQ)
- Cation exchange capacity
- Clay
- Coarse fragments
- Electrical conductivity
- Organic carbon (C)
- Organic matter
- pH
- Phosphorus (P)
- Sand
- Silt
- Total carbon (C)
- Total nitrogen (N)
- Water retention gravimetric
- Water retention volumetric

_Note: As of February 2025, this list has been expanded with exchangeable bases, acidity, gypsum, soluble salts, a range of microbutrients resp. trace metal elements, and hydraulic conductivity (see  https://doi.org/10.17027/isric-wdcsoils-1nh7-zr51).


New data model
-------------

The key modification in the new data model is the conditioning of analytical
methods to the observation. A collection of method options only applies in the
scope of an observation (see Procedures Manual). Realising this
conditioning results in a complex data model, where natural keys guaranteeing
the observation referenced by a result concurs with the observation in the
associated analytical method. In practice this guarantee is enforced by the
field `observation_layer_numeric_id` in the table `method_option_standard`,
where it is part of both the foreign key to the `method_standard` table and the
`method_option` table.

The complexity of the new data model hints at a sub-optimal arrangement.
However, a simpler data model with the current data would require code
constraints (e.g. triggers) to enforce the conditions described above. A
complete optimal solution would require the redesign of other segments of the
database, with a doubtful cost-benefit ratio.

```mermaid
erDiagram
  thes_method_key {
    INTEGER thes_method_key_id
    VARCHAR key
  }
  thes_method_value {
    INTEGER thes_method_value_id
    VARCHAR value
  }
  method_option {
    INTEGER thes_method_value_id
    INTEGER thes_method_key_id
    INTEGER observation_layer_numeric_id
    SMALLINT code
  }
  method_standard {
    INTEGER method_source_id
    INTEGER observation_layer_numeric_id
  }
  method_option_standard {
    INTEGER method_source_id
    INTEGER observation_layer_numeric_id
    INTEGER thes_method_value_id
    INTEGER thes_method_key_id
  }
  method_source {
    INTEGER method_source_id
    VARCHAR method_source_code
    VARCHAR source_analytical_method_name
    VARCHAR source_analytical_method_code
  }
  observation_layer_numeric {
    INTEGER observation_layer_numeric_id
    INTEGER property_layer_numeric_id
    INTEGER procedure_layer_numeric_id
    INTEGER unit_id
    VARCHAR code
    SMALLINT decimals
    NUMERIC minimum
    NUMERIC maximum
    NUMERIC accuracy
    SMALLINT priority
    BOOLEAN progress_name
    BOOLEAN progress_value
    BOOLEAN progress_method
    BOOLEAN distribute
    BOOLEAN sg_variable
    BOOLEAN gsm
    BOOLEAN gfsd
    VARCHAR category
    VARCHAR desc_attribute_standard_id
  }
  method_option ||--o{ observation_layer_numeric : observation_layer_numeric_id_fkey
  method_option ||--o{ thes_method_key : thes_method_key_id_fkey
  method_option ||--o{ thes_method_value : thes_method_value_id_fkey
  method_standard ||--o{ observation_layer_numeric : observation_layer_numeric_id_fkey
  method_standard ||--o{ method_source : method_source_id_fkey
  method_option_standard ||--o{ method_standard : method_standard_fkey
  method_option_standard ||--o{ method_option : method_option_fkey
  method_option_standard ||--o{ method_standard : method_standard_fkey
  method_option_standard ||--o{ method_option : method_option_fkey
  method_option_standard ||--o{ method_option : method_option_fkey
```

### Tables in the new data model

- `thes_method_value`: Thesaurus of values that match the keys used to define an
  analytical method. E.g. "natural clod" for the "sample type" key. See
  Procedures Manual for details.

- `thes_method_key`: Thesaurus of keys used to define an analytical method. E.g.
  "sample type", "size", "ratio". See Procedures Manual for details.

- `thes_method_option`: Encodes the possible combinations of key-value pairs for
  each numerical observation on layers. Note that only a small sub-set of
  properties (and respective observations) can be associated with method
  options.

- `method_source`: Analytical methods descriptions as defined in the respective
  source databases (i.e. prior to standardisation). This table was imported from
  the old data model "as is" with the addition of a synthetic primary key. The
  records in this table remain essential to identify the method referred by each
  result.

- `method_standard`: Distinguishes each source method by the particular
  observation to which it applies. Can be regarded as a specialised sub-set of
  the source method. Each record corresponds to a collection of key-value pairs
  in the `method_option` table for a single observation. Results for numerical
  observations reference this table to identify the corresponding analytical
  method.

- `method_option_standard`: Many-to-many relationship between method standard
  and method option. Determines the exact collection of key-value option pairs
  that constitute a standard method. The standard method is a specialisation of
  the source method for a specific observation.

Dashboard
----------

This dashboard provides some insight to the data recorded in the new data model
for the current methods and their options.

- [**Method Options**](https://dashboards.isric.org/superset/dashboard/36):
  reflects the tables presented in the 2023 Procedures Manual (https://doi.org/10.17027/isric-1dq0-1m83). It
  lists the method options available for each observation.


