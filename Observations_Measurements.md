---
title: Observations & Measurements
author: Luís Moreira de Sousa luis[dot]desousa[at]isric[dot]org 
date: 2023-12-08
---

Observations & Measurements
===========================

Observations & Measurements (O&M) is an ISO standard developed within the OGC,
initially published in 2011. It proposes a domain model for the storage and
exchange of data representing measurements of natural phenomena. All relevant
ontologies and domain models in the soil context are specialisations of O&M:
ANZSoilML, ISO-28258, INSPIRE Soil Theme and OGC Soil Interchange Experiment. An
account of O&M and its relevance to soil data can be found in [Section
2](https://iso28258.isric.org/#sec:28258) of the documentation to ISRIC's data
model implementing ISO-28258. 

Measurements represent by far the largest volume of data in WoSIS, reaching tens
of millions of records. As described in the [Features](Features.md) section, in
WoSIS only the concepts of Profile and Layer are subject to observation, and
thus all observable properties relate to either one of these.

Issues with the original data model
-----------------------------------

The [original data model](https://www.isric.org/sites/default/files/WOSISprocedureManual_2020nov17web.pdf) was rather diffuse regarding O&M, lacking a clear
identification of the foundational concept of Observation. Relevant elements
were found in four different tables:

- `desc_attribute`
- `desc_attribute_agg`
- `desc_attribute_standard`  
- `descriptor`

All these tables yielded a high degree of functional dependencies, in most cases
attempting to capture various concepts simultaneously. However only
`desc_attribute_standard` included a reference to units of measurement. This
table was thus taken as main reference for the development of a new data model.

The tables `desc_attribute_agg` and `descriptor` both related to
`desc_attribute_standard` and `desc_method_standard` creating what effectively
was a circular relationship. Great effort was made to unravel the nature and
role of this circular relationship, involving various elements of the ISRIC
staff. As with other sections of the original data model, a clear understanding
could not be established, resulting in the complete removal of the two tables
from the new data model.

The lack of controlled content was a further issue, also common to the remainder
of the data model, but especially critical in this section. All measurements
were recorded in free text fields without any control mechanisms (tables
`profile_layer_attribute` and `profile_attribute`). The usual problems with term
duplication were pervasive. 

New data model
--------------

The new data model starts by identifying four different types of observations.
They undo part of the functional dependencies in the original
`desc_attribute_standard` table:
- Descriptive observations on Profile
- Descriptive observations on Layer
- Numeric observations on Profile 
- Numeric observations on Layer

Descriptive observations yield a thesaurus term as result (e.g. soil colour), in
contrast to those numerical in nature (e.g. pH). Results also differ whether
they refer to a profile or a layer. In most cases, properties valid to Profile
are not applicable to Layer and vice-versa. 

### Descriptive Observations

Controlled terms applicable to descriptive observations are maintained in the
tables `thes_layer_desc` and `thes_profile_desc`. Each 
term may refer to a particular publication (`thes_reference` table) from which
it is derived or adopted. The tables `observation_layer_desc_item` and
`observation_profile_desc_item` determine which terms may be used with which
observation. This structure allows for the maintenance of all measurement
vocabularies in a single thesaurus table.

The descriptive result tables refer to :
- the feature of interest (a profile or layer) 
- the observation
- a term in the thesaurus
- a laboratory

The foreign key from the result table to the controlled thesaurus (e.g.
`observation_layer_desc_item`) is a composite one, guaranteeing only those terms
associated with the observation in question are used. Whereas in most cases
descriptive observations refer to pedo-genetic processes assessed in the field,
there are measurements of this kind in WoSIS referring to a laboratory. 

The Observation tables of this type do not refer to a unit of measurement, nor
to a procedure. A direct implementation of the O&M model would yield the
reference publication as procedure. However, the freedom allowed in the
original data model prevented a similar structure, with a reference to the
publication closer to the result itself (previously in the enigmatic
`descriptor` table).   

```mermaid
erDiagram
  property_profile_desc {
    INTEGER property_profile_desc_id
    VARCHAR name
    VARCHAR description
  }
  result_profile_desc {
    INTEGER result_profile_desc_id
    INTEGER profile_id
    INTEGER observation_profile_desc_id
    INTEGER thes_profile_desc_id
    INTEGER laboratory_id
  }
  observation_profile_desc_item {
    INTEGER thes_profile_desc_id
    INTEGER observation_profile_desc_id
  }
  laboratory {
    INTEGER laboratory_id
    VARCHAR laboratory_code
    VARCHAR laboratory
    CHAR country_id
    VARCHAR city
    VARCHAR postal_code
    VARCHAR street_name
    VARCHAR street_number
    INTEGER reference_id
  }
  observation_profile_desc {
    INTEGER observation_profile_desc_id
    INTEGER property_profile_desc_id
    VARCHAR code
    SMALLINT priority
    BOOLEAN progress_name
    BOOLEAN progress_value
    BOOLEAN progress_method
    BOOLEAN distribute
    BOOLEAN sg_variable
    BOOLEAN gsm
    BOOLEAN gfsd
    VARCHAR desc_attribute_standard_id
  }
  thes_profile_desc {
    INTEGER thes_profile_desc_id
    VARCHAR item
    INTEGER thes_reference_id
    VARCHAR description
    VARCHAR explanation
  }
  thes_document {
    INTEGER thes_document_id
    VARCHAR title
    SMALLINT year
  }
  thes_reference {
    INTEGER thes_reference_id
    VARCHAR label
    INTEGER thes_document_id
    SMALLINT page
    SMALLINT figure
  }
  profile {
    INTEGER dataset_id
    INTEGER site_id
    INTEGER thesaurus_trust_id
    TEXT profile_code
    SMALLINT year
    SMALLINT month
    SMALLINT day
    INTEGER profile_old_id
    INTEGER profile_id
  }
  observation_profile_desc ||--o{ property_profile_desc : property_profile_desc_id_fkey
  result_profile_desc ||--o{ laboratory : laboratory_id_fkey
  result_profile_desc ||--o{ observation_profile_desc_item : observation_profile_desc_id_fkey
  result_profile_desc ||--o{ observation_profile_desc_item : observation_profile_desc_id_fkey
  result_profile_desc ||--o{ profile : profile_id_fkey
  observation_profile_desc_item ||--o{ thes_profile_desc : thes_profile_desc_id_fkey
  observation_profile_desc_item ||--o{ observation_profile_desc : observation_profile_desc_id_fkey
  thes_profile_desc ||--o{ thes_reference : thes_reference_id_fkey
  thes_reference ||--o{ thes_document : thes_document_id_fkey
```

```mermaid
erDiagram
  observation_layer_desc {
    INTEGER observation_layer_desc_id
    INTEGER property_layer_desc_id
    VARCHAR code
    SMALLINT priority
    BOOLEAN progress_name
    BOOLEAN progress_value
    BOOLEAN progress_method
    BOOLEAN distribute
    BOOLEAN sg_variable
    BOOLEAN gsm
    BOOLEAN gfsd
    VARCHAR desc_attribute_standard_id
  }
  property_layer_desc {
    INTEGER property_layer_desc_id
    VARCHAR name
    VARCHAR description
  }
  laboratory {
    INTEGER laboratory_id
    VARCHAR laboratory_code
    VARCHAR laboratory
    CHAR country_id
    VARCHAR city
    VARCHAR postal_code
    VARCHAR street_name
    VARCHAR street_number
    INTEGER reference_id
  }
  observation_layer_desc_item {
    INTEGER thes_layer_desc_id
    INTEGER observation_layer_desc_id
  }
  result_layer_desc {
    INTEGER result_layer_desc_id
    INTEGER layer_id
    INTEGER observation_layer_desc_id
    INTEGER thes_layer_desc_id
    INTEGER laboratory_id
  }
  layer {
    INTEGER layer_id
    INTEGER profile_id
    SMALLINT layer_number
    VARCHAR layer_name
    SMALLINT upper_depth
    SMALLINT lower_depth
    BOOLEAN organic_surface
  }
  thes_document {
    INTEGER thes_document_id
    VARCHAR title
    SMALLINT year
  }
  thes_reference {
    INTEGER thes_reference_id
    VARCHAR label
    INTEGER thes_document_id
    SMALLINT page
    SMALLINT figure
  }
  thes_layer_desc {
    INTEGER thes_layer_desc_id
    VARCHAR item
    INTEGER thes_reference_id
    VARCHAR description
    VARCHAR explanation
  }
  observation_layer_desc ||--o{ property_layer_desc : property_layer_desc_id_fkey
  result_layer_desc ||--o{ observation_layer_desc_item : observation_layer_desc_id_fkey
  result_layer_desc ||--o{ observation_layer_desc_item : observation_layer_desc_id_fkey
  result_layer_desc ||--o{ layer : layer_id_fkey
  result_layer_desc ||--o{ laboratory : laboratory_id_fkey
  observation_layer_desc_item ||--o{ thes_layer_desc : thes_layer_desc_id_fkey
  observation_layer_desc_item ||--o{ observation_layer_desc : observation_layer_desc_id_fkey
  thes_layer_desc ||--o{ thes_reference : thes_reference_id_fkey
  thes_reference ||--o{ thes_document : thes_document_id_fkey
```

### Numerical Observations

The new data model for numerical observations yields a more traditional
structure, with clear identification of Property, Procedure, Unit of
Measurement, Observation and Result. The most laborious
and impactful work was the removal of the functional dependency between
Property and Procedure in the original `desc_attribute_standard` table. An extensive map was developed, identifying unequivocally the property and
procedure in each record of the original `desc_attribute_standard` table.

The new result tables retain the references to the laboratory and the analytical
method, both recycled from the `descriptor` table in the original data model.
Some degree of overlap is recognised between the concept of Procedure and that
of Analytical Method. This is one of the most important open questions regarding the data model,
discussed in more detail in the [Analytical Methods](Methods.md) section. 


```mermaid
erDiagram
  thesaurus_trust {
    VARCHAR value
    VARCHAR definition
    INTEGER thesaurus_trust_id
  }
  unit {
    INTEGER unit_id
    VARCHAR symbol
    VARCHAR description
  }
  result_profile_numeric {
    INTEGER result_profile_numeric_id
    NUMERIC value
    INTEGER profile_id
    INTEGER observation_profile_numeric_id
    INTEGER thesaurus_trust_id
    VARCHAR accuracy
  }
  observation_profile_numeric {
    INTEGER observation_profile_numeric_id
    INTEGER property_profile_numeric_id
    INTEGER unit_id
    VARCHAR code
    SMALLINT decimals
    NUMERIC minimum
    NUMERIC maximum
    NUMERIC accuracy
    SMALLINT priority
    BOOLEAN progress_name
    BOOLEAN progress_value
    BOOLEAN progress_method
    BOOLEAN distribute
    BOOLEAN sg_variable
    BOOLEAN gsm
    BOOLEAN gfsd
    VARCHAR category
    VARCHAR desc_attribute_standard_id
  }
  property_profile_numeric {
    INTEGER property_profile_numeric_id
    VARCHAR name
    VARCHAR description
  }
  profile {
    INTEGER dataset_id
    INTEGER site_id
    INTEGER thesaurus_trust_id
    TEXT profile_code
    SMALLINT year
    SMALLINT month
    SMALLINT day
    INTEGER profile_old_id
    INTEGER profile_id
  }
  observation_profile_numeric ||--o{ unit : unit_id_fkey
  observation_profile_numeric ||--o{ property_profile_numeric : property_profile_numeric_id_fkey
  result_profile_numeric ||--o{ observation_profile_numeric : observation_profile_numeric_id_fkey
  result_profile_numeric ||--o{ profile : profile_id_fkey
  result_profile_numeric ||--o{ thesaurus_trust : thesaurus_trust_id_fkey
```

```mermaid
erDiagram
  result_layer_numeric {
    INTEGER result_layer_numeric_id
    NUMERIC value
    INTEGER layer_id
    INTEGER observation_layer_numeric_id
    INTEGER method_source_id
    INTEGER laboratory_id
    INTEGER thesaurus_trust_id
    VARCHAR accuracy
  }
  thesaurus_trust {
    VARCHAR value
    VARCHAR definition
    INTEGER thesaurus_trust_id
  }
  property_layer_numeric {
    INTEGER property_layer_numeric_id
    VARCHAR name
    VARCHAR description
  }
  unit {
    INTEGER unit_id
    VARCHAR symbol
    VARCHAR description
  }
  method_standard {
    INTEGER method_source_id
    INTEGER observation_layer_numeric_id
  }
  laboratory {
    INTEGER laboratory_id
    VARCHAR laboratory_code
    VARCHAR laboratory
    CHAR country_id
    VARCHAR city
    VARCHAR postal_code
    VARCHAR street_name
    VARCHAR street_number
    INTEGER reference_id
  }
  observation_layer_numeric {
    INTEGER observation_layer_numeric_id
    INTEGER property_layer_numeric_id
    INTEGER procedure_layer_numeric_id
    INTEGER unit_id
    VARCHAR code
    SMALLINT decimals
    NUMERIC minimum
    NUMERIC maximum
    NUMERIC accuracy
    SMALLINT priority
    BOOLEAN progress_name
    BOOLEAN progress_value
    BOOLEAN progress_method
    BOOLEAN distribute
    BOOLEAN sg_variable
    BOOLEAN gsm
    BOOLEAN gfsd
    VARCHAR category
    VARCHAR desc_attribute_standard_id
  }
  procedure_layer_numeric {
    INTEGER procedure_layer_numeric_id
    VARCHAR name
    VARCHAR description
  }
  layer {
    INTEGER layer_id
    INTEGER profile_id
    SMALLINT layer_number
    VARCHAR layer_name
    SMALLINT upper_depth
    SMALLINT lower_depth
    BOOLEAN organic_surface
  }
  observation_layer_numeric ||--o{ property_layer_numeric : property_layer_numeric_id_fkey
  observation_layer_numeric ||--o{ unit : unit_id_fkey
  observation_layer_numeric ||--o{ procedure_layer_numeric : procedure_layer_numeric_id_fkey
  result_layer_numeric ||--o{ thesaurus_trust : thesaurus_trust_id_fkey
  result_layer_numeric ||--o{ method_standard : method_standard_fkey
  result_layer_numeric ||--o{ layer : layer_id_fkey
  result_layer_numeric ||--o{ method_standard : method_standard_fkey
  result_layer_numeric ||--o{ laboratory : laboratory_id_fkey
  result_layer_numeric ||--o{ observation_layer_numeric : observation_layer_numeric_id_fkey
```

### Tables in the new data model

- `laboratory`: Laboratory where a soil sample has been analysed (clean).
 	
- `observation_layer_desc`: 	A soil layer descriptive/qualitative property, whose result is an item from a controlled thesaurus. E.g. "cemented" for a soil compaction property.

- `observation_layer_desc_item`: 	Many-to-many relationship between `thes_layer_desc` and `observation_layer_desc`. Defines which thesaurus items may be used with which observations. Also serves as reference to results of descriptive observations on Layer.

- `observation_layer_numeric`: 	A soil layer property observation according to a procedure, whose result is numerical, e.g. soil pH in water solution.

- `observation_profile_desc`: 	A soil profile descriptive/qualitative property, whose result is an item from a controlled thesaurus. E.g. "outcrop" for a rockiness property.

- `observation_profile_desc_item`: 	Many-to-many relationship between `thes_profile_desc` and `observation_profile_desc`. Defines which thesaurus items may be used with which observations. Also serves as reference to results of descriptive observations on Profile.

- `observation_profile_numeric`: 	A soil profile property observation according to a procedure, whose result is numerical, e.g. soil pH in water solution.

- `observation_sample_spectral`: 	A sample observation conducted with a spectroscopy instrument, whose result is a discrete spectrum of values. Note that for the moment no instrument is specified (equivalent to Procedure in O&M and Sensor in OMS).

- `procedure_layer_numeric`: 	A procedure employed to assess a soil layer observation which produces numerical results, e.g. dry combustion of the soil.

- `property_layer_desc`: 	A property of the soil layer whose observations produce descriptive or qualitative results, e.g. soil colour. In most cases these properties are observed during field work.

- `property_layer_numeric`: 	A property of the soil layer whose observations produce numerical results, e.g. soil pH. These are usually physical or chemical properties assessed in a laboratory.

- `property_profile_desc`: 	A property of the soil profile whose observations produce descriptive or qualitative results, e.g. rockiness. In most cases these properties are observed during field work.

- `property_profile_numeric`: 	A property of the soil profile whose observations produce numerical results, e.g. soil pH. These are usually physical or chemical properties assessed in a laboratory.

- `result_layer_desc`: 	Results of descriptive and qualitative observations on layers.

- `result_layer_numeric`: 	Results of numerical observations on layers.

- `result_profile_desc`: 	Results of descriptive and qualitative observations on profiles.

- `result_profile_numeric`: 	Results of numerical observations on profiles.

- `result_sample_spectral`: 	Results of spectral observations on soil samples.

- `thes_document`: A report, article or other kind of publication from which a thesaurus was digitised. 

- `thes_layer_desc`: 	An item of a controlled thesaurus for descriptive/qualitative observations for Layer.

- `thes_profile_desc`: 	An item of a controlled thesaurus for descriptive/qualitative observations for Profile.

- `thes_reference`: 	A specific element within a document from which a controlled thesaurus was digitised. E.g. a table in the FAO Guidelines for Soil Description.

- `unit`: 	Units of measurement for numerical observations.


