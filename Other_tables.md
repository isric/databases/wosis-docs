---
title: Other tables
author: Luís Moreira de Sousa luis[dot]de[dot]sousa[at]protonmail[dot]ch 
date: 2023-08-30
---
Other tables
============

A small number of tables are not referenced in the main documentation, as their
role is too marginal, or unrelated to the core elements of the data model. They
can be regarded as additional meta-data sources. These tables were transferred
to the new data model without any relevant modifications.

### Tables in the new data model

```mermaid
erDiagram
  reference {
    INTEGER reference_id
    VARCHAR reference_type
    VARCHAR isbn
    VARCHAR isn
    VARCHAR title
    VARCHAR issue
    SMALLINT publication_year
    VARCHAR publisher
    VARCHAR url
  }
  conversion {
    INTEGER conversion_id
    INTEGER unit_from
    INTEGER unit_to
    VARCHAR operation
    SMALLINT value
  }
  profile {
    INTEGER dataset_id
    INTEGER site_id
    INTEGER thesaurus_trust_id
    TEXT profile_code
    SMALLINT year
    SMALLINT month
    SMALLINT day
    INTEGER profile_old_id
    INTEGER profile_id
  }
  reference_profile {
    INTEGER reference_id
    INTEGER profile_id
    VARCHAR reference_page
    VARCHAR reference_profile_code
  }
  site_covariate {
    INTEGER site_id
    NULL geom
    TEXT biome_name
    TEXT eco_name
    TEXT gelu_ef
    TEXT gelu_ef_bio
    TEXT gelu_ef_lf
    TEXT gelu_ef_lit
    TEXT gelu_ef_glc
    TEXT gelu_elu
    TEXT gelu_elu_bio
    TEXT gelu_elu_lf
    TEXT gelu_elu_lit
    TEXT gelu_elu_glc
    TEXT gelu_elu_site
  }
  site {
    INTEGER site_id
    NULL geom
    REAL geom_accuracy
    VARCHAR geom_accuracy_type
    BOOLEAN hide
  }
  reference_profile ||--o{ profile : reference_profile_fkey
  reference_profile ||--o{ reference : reference_profile_reference_id_fkey
  site_covariate ||--o{ site : site_id_fkey
```

### Tables in new data model 

- `conversion`: Conversion factor between different units. 
- `reference`: List of references to source materials managed in WoSIS. 
- `reference_profile`: List of references by imported dataset and their profiles.
- `site_covariate`: Site covariates from diverse sources



