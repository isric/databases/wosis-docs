WoSIS Data Model 2023
=====================
_Prep. by Luis M. de Sousa (ISRIC)_

Contents:

- [**Features and Meta-data**](Features.md)
- [**Dataset and Profile**](Dataset_Profile.md)
- [**Site**](Site.md)
- [**Sample and Layer**](Sample_Layer.md)
- [**Classification Systems**](Classification.md)
- [**Analytical Methods**](Methods.md)
- [**Observations & Measurements**](Observations_Measurements.md)
- [**Other tables**](Other_tables.md)
- [**Source measurements**](Source_measurements.md)


Futher reading: 
[FAQ - WoSIS](https://www.isric.org/explore/wosis/faq-wosis) 

![WoSIS](https://www.isric.org/sites/default/files/wosis_Picture1.png)

