---
title: Source measurements
author: Luís Moreira de Sousa luis[dot]de[dot]sousa[at]isric[dot]org 
date: 2023-10-11
---

Source measurements
===================

The table `profile_layer_attribute` in the original data model ([Ribeiro et al. 2020](http://dx.doi.org/10.17027/isric-wdc-2020-01)) contained some
35 million records, with observations results stored in two different fields,
`standard_value` and `source_value`. According to the legacy documentation the
latter stores results as reported in the source dataset(s), whereas the former
stores the "standardised" result (i.e. for those properties that have been standardised so far). 

The suffix `_source` appeared throughout the original data model, with various
fields and tables reporting diverse information on the source datasets. As far
as it could be established, none of these assets were in active use. They added
a relevant layer of unnecessary complexity that was important to remove. They
were not deleted but rather transferred to a specific database schema:
`wosis_source`. They report exclusively to numerical observations and
measurements on the Layer entity. To this schema were also transferred the result
records with `NULL` values in the `source_value` field. Below is a brief
overview of this schema.

### Tables in the `wosis_source` schema

```mermaid
erDiagram
  result_layer_numeric {
    INTEGER result_layer_numeric_id
    VARCHAR value
    INTEGER layer_id
    INTEGER observation_layer_numeric_id
    INTEGER laboratory_id
    INTEGER thesaurus_trust_id
    VARCHAR accuracy
  }
  observation_profile_numeric {
    INTEGER observation_profile_numeric_id
    INTEGER property_profile_numeric_id
    INTEGER unit_id
    VARCHAR code
    SMALLINT decimals
    NUMERIC minimum
    NUMERIC maximum
    NUMERIC accuracy
    SMALLINT priority
    BOOLEAN progress_name
    BOOLEAN progress_value
    BOOLEAN progress_method
    BOOLEAN distribute
    BOOLEAN sg_variable
    BOOLEAN gsm
    BOOLEAN gfsd
    VARCHAR category
    VARCHAR desc_attribute_standard_id
  }
  observation_layer_desc {
    INTEGER observation_layer_desc_id
    INTEGER property_layer_desc_id
    VARCHAR code
    SMALLINT priority
    BOOLEAN progress_name
    BOOLEAN progress_value
    BOOLEAN progress_method
    BOOLEAN distribute
    BOOLEAN sg_variable
    BOOLEAN gsm
    BOOLEAN gfsd
    VARCHAR desc_attribute_standard_id
  }
  profile {
    INTEGER dataset_id
    INTEGER site_id
    INTEGER thesaurus_trust_id
    TEXT profile_code
    SMALLINT year
    SMALLINT month
    SMALLINT day
    INTEGER profile_old_id
    INTEGER profile_id
  }
  dataset {
    TEXT dataset_code
    TEXT version
    TEXT dataset_progress
    TEXT dataset_type
    SMALLINT dataset_rank
    BOOLEAN licence_wosis
    BOOLEAN licence_soilgrids
    TEXT licence_file
    TEXT legal_constraints
    DATE publication_date
    TEXT project
    INTEGER dataset_id
  }
  laboratory {
    INTEGER laboratory_id
    VARCHAR source_laboratory_name
    INTEGER laboratory_standard_id
  }
  thesaurus_trust {
    VARCHAR value
    VARCHAR definition
    INTEGER thesaurus_trust_id
  }
  observation_layer_numeric {
    INTEGER observation_layer_numeric_id
    INTEGER property_layer_numeric_id
    INTEGER procedure_layer_numeric_id
    INTEGER unit_id
    VARCHAR code
    SMALLINT decimals
    NUMERIC minimum
    NUMERIC maximum
    NUMERIC accuracy
    SMALLINT priority
    BOOLEAN progress_name
    BOOLEAN progress_value
    BOOLEAN progress_method
    BOOLEAN distribute
    BOOLEAN sg_variable
    BOOLEAN gsm
    BOOLEAN gfsd
    VARCHAR category
    VARCHAR desc_attribute_standard_id
  }
  result_layer_desc {
    INTEGER result_layer_desc_id
    VARCHAR value
    INTEGER layer_id
    INTEGER observation_layer_desc_id
    INTEGER laboratory_id
    INTEGER thesaurus_trust_id
  }
  observation_profile_desc {
    INTEGER observation_profile_desc_id
    INTEGER property_profile_desc_id
    VARCHAR code
    SMALLINT priority
    BOOLEAN progress_name
    BOOLEAN progress_value
    BOOLEAN progress_method
    BOOLEAN distribute
    BOOLEAN sg_variable
    BOOLEAN gsm
    BOOLEAN gfsd
    VARCHAR desc_attribute_standard_id
  }
  layer {
    INTEGER layer_id
    INTEGER profile_id
    SMALLINT layer_number
    VARCHAR layer_name
    SMALLINT upper_depth
    SMALLINT lower_depth
    BOOLEAN organic_surface
  }
  laboratory {
    INTEGER laboratory_id
    VARCHAR laboratory_code
    VARCHAR laboratory
    CHAR country_id
    VARCHAR city
    VARCHAR postal_code
    VARCHAR street_name
    VARCHAR street_number
    INTEGER reference_id
  }
  unit {
    INTEGER unit_id
    VARCHAR symbol
    VARCHAR description
  }
  desc_attribute {
    INTEGER desc_attribute_id
    INTEGER dataset_id
    TEXT schema_name
    TEXT table_name
    TEXT column_name
    TEXT source_attribute_name
    TEXT source_attribute_description
    INTEGER source_attribute_unit_id
    TEXT source_attribute_type
    TEXT source_attribute_domain
    TEXT attribute_type
    TEXT conversion
    TEXT sql_insert
    INTEGER number_rows_source
    INTEGER number_rows_inserted
    INTEGER number_rows_standard
    ARRAY conflict_source_domain
    TEXT user_name
    DATE start_date
    TIME start_time
    TIME proce_time
    TEXT client_addr
    TEXT server_addr
    TEXT note
  }
  layer {
    INTEGER layer_id
    VARCHAR layer_name_source
    VARCHAR upper_depth_source
    VARCHAR lower_depth_source
  }
  result_profile_numeric {
    INTEGER result_profile_numeric_id
    VARCHAR value
    INTEGER profile_id
    INTEGER observation_profile_numeric_id
    INTEGER thesaurus_trust_id
    VARCHAR accuracy
  }
  result_profile_desc {
    INTEGER result_profile_desc_id
    VARCHAR value
    INTEGER profile_id
    INTEGER observation_profile_desc_id
    INTEGER laboratory_id
    INTEGER thesaurus_trust_id
  }
  desc_attribute ||--o{ unit : desc_attribute_source_attribute_unit_fkey
  desc_attribute ||--o{ dataset : desc_attribute_dataset_id_fkey
  laboratory ||--o{ laboratory : laboratory_standard_id_fkey
  layer ||--o{ layer : layer_id_fkey
  result_layer_desc ||--o{ laboratory : laboratory_id_fkey
  result_layer_desc ||--o{ observation_layer_desc : observation_layer_desc_id_fkey
  result_layer_desc ||--o{ layer : layer_id_fkey
  result_layer_desc ||--o{ thesaurus_trust : thesaurus_trust_id_fkey
  result_layer_numeric ||--o{ observation_layer_numeric : observation_layer_numeric_id_fkey
  result_layer_numeric ||--o{ laboratory : laboratory_id_fkey
  result_layer_numeric ||--o{ thesaurus_trust : thesaurus_trust_id_fkey
  result_layer_numeric ||--o{ layer : layer_id_fkey
  result_profile_desc ||--o{ observation_profile_desc : observation_profile_desc_id_fkey
  result_profile_desc ||--o{ laboratory : laboratory_id_fkey
  result_profile_desc ||--o{ profile : profile_id_fkey
  result_profile_desc ||--o{ thesaurus_trust : thesaurus_trust_id_fkey
  result_profile_numeric ||--o{ observation_profile_numeric : observation_profile_numeric_id_fkey
  result_profile_numeric ||--o{ profile : profile_id_fkey
  result_profile_numeric ||--o{ thesaurus_trust : thesaurus_trust_id_fkey
```


- `desc_attribute`:  Description of all the soil properties for each dataset that has been imported in WoSIS.
- `laboratory`:  Listing of laboratories where soil samples have been analysed.
- `layer`:  Layer. A vertical profile element considered to have an heterogeneous composition. This table contains non-harmonised information present in the original data source.
- `result_layer_desc`:  Results of descriptive observations on layers.
- `result_layer_numeric`:  Results of numerical observations on layers.
- `result_profile_desc`:  Results of descriptive observations on profiles.
- `result_profile_numeric`:  Results of numerical observations on profiles.
- `unit`:  Units of measurement for the `desc_attribute` table.

