---
title: Dataset & Profile
author: Luís Moreira de Sousa luis[dot]desousa[at]isric[dot]org 
date: 2023-10-19
---

Dataset & Profile
=================

The concept of Dataset was carried over from the legacy data model without
change. It is essentially a meta-data aggregator, primarily identifying a
source of data loaded into WoSIS. A dataset may include data collected in
different surveys, in different regions and at different points in time.
Therefore no assumptions in this regard should be generalised.

A Profile is an ordered and continuous collection of soil horizons, describing
and/or characterizing the soil at a certain location, from the surface
downwards. At present the concept of Profile in WoSIS matches that specified in
the ISO-28258 standard, thus common to INSPIRE, ANZSoilML and similar models in
the same family. All measurements recorded in WoSIS refer to a profile, either
directly or through a profile element.

New data model
--------------

The most important modifications in the new data model are the
identification of the features of interest and related entities. Another
relevant modification is the presence of date fields (`day`, `month` and
`year`) in the new `profile` table, thus no longer associated with geographic
location. 

```sql
erDiagram
  dataset {
    TEXT dataset_code
    TEXT version
    TEXT dataset_progress
    TEXT dataset_type
    SMALLINT dataset_rank
    BOOLEAN licence_wosis
    BOOLEAN licence_soilgrids
    TEXT licence_file
    TEXT legal_constraints
    DATE publication_date
    TEXT project
    INTEGER dataset_id
  }
  thesaurus_trust {
    VARCHAR value
    VARCHAR definition
    INTEGER thesaurus_trust_id
  }
  site {
    INTEGER site_id
    NULL geom
    REAL geom_accuracy
    VARCHAR geom_accuracy_type
    BOOLEAN hide
  }
  profile {
    INTEGER dataset_id
    INTEGER site_id
    INTEGER thesaurus_trust_id
    TEXT profile_code
    SMALLINT year
    SMALLINT month
    SMALLINT day
    INTEGER profile_old_id
    INTEGER profile_id
  }
  profile ||--o{ dataset : profile_dataset_id_fkey
  profile ||--o{ thesaurus_trust : fk_thesaurus_trust
  profile ||--o{ site : profile_site_id_fkey
```

### Tables in the new data model

- `dataset`: Describes datasets imported to the WoSIS database or datasets that
  are known to contain some of the profiles in the imported datasets.

- `profile`: Ordered and continuous collection of soil horizons or layers,
  describing and/or characterising the soil at a certain location, from the
surface downwards.

- `site`: Location in which a soil profile is described or otherwise
  characterised. The same site can be visited at different moments in time and
feature in different datasets. Thus various different profiles may exist for a
site.

- `thesaurus_trust`: Level of trust on the associated record.





 

